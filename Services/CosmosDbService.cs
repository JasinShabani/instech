﻿using Domain.Interfaces.Services;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CosmosDbService<T> : ICosmosDbService<T>
    {
        private readonly Container _container;
        private readonly Func<T, string> _getPartitionKeyFunc; // Function to retrieve the partition key

        public CosmosDbService(Container container, Func<T, string> getPartitionKeyFunc)
        {
            _container = container ?? throw new ArgumentNullException(nameof(container));
            _getPartitionKeyFunc = getPartitionKeyFunc ?? throw new ArgumentNullException(nameof(getPartitionKeyFunc));
        }

        public async Task<IEnumerable<T>> GetItemsAsync(string queryString)
        {
            var query = _container.GetItemQueryIterator<T>(new QueryDefinition(queryString));
            var results = new List<T>();
            while (query.HasMoreResults)
            {
                var response = await query.ReadNextAsync();
                results.AddRange(response.ToList());
            }
            return results;
        }

        public async Task<T> GetItemAsync(string id)
        {
            try
            {
                var response = await _container.ReadItemAsync<T>(id, new PartitionKey(id));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return default; // Change to null if T is a reference type
            }
        }

        public async Task<T> AddItemAsync(T item)
        {
            var partitionKey = _getPartitionKeyFunc(item);
            var result = await _container.CreateItemAsync(item, new PartitionKey(partitionKey));
            return result;
        }

        public Task UpdateItemAsync(string id, T item)
        {
            var partitionKey = _getPartitionKeyFunc(item);
            return _container.UpsertItemAsync(item, new PartitionKey(partitionKey));
        }

        public Task DeleteItemAsync(string id)
        {
            return _container.DeleteItemAsync<T>(id, new PartitionKey(id));
        }
    }
}
