﻿using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;

namespace Services
{
    public class AuditorService : IAuditorService
    {
        private readonly IAuditorRepository _auditorRepository;

        public AuditorService(IAuditorRepository auditorRepository) {
            _auditorRepository = auditorRepository;
        }

        public void AuditClaim(string id, string httpRequestType)
        {
            _auditorRepository.AuditClaim(id, httpRequestType);
        }

        public void AuditCover(string id, string httpRequestType)
        {
            _auditorRepository.AuditCover(id, httpRequestType);
        }
    }
}
