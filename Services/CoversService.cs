﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;
using Domain.Models.Enums;

namespace Services
{
    public class CoversService : ICoversService
    {
        private readonly IMapper _mapper;
        private readonly ICosmosDbService<Cover> _cosmosDbService;

        public CoversService(IMapper mapper,
                            ICosmosDbService<Cover> cosmosDbService)
        {
            _mapper = mapper;
            _cosmosDbService = cosmosDbService;
        }

        public async Task<decimal> ComputePremiumAsync(DateTime startDate, DateTime endDate, ECoverType coverType)
        {
            return ComputePremium(startDate, endDate, coverType);
        }

        public async Task<IEnumerable<CoverResponse>> GetAsync()
        {
            var response = await _cosmosDbService.GetItemsAsync("SELECT * FROM c");
            return _mapper.Map<IEnumerable<CoverResponse>>(response);
        }

        public async Task<CoverResponse> GetAsync(string id)
        {
            var response = await _cosmosDbService.GetItemAsync(id);
            if (response != null)
            {
                return _mapper.Map<CoverResponse>(response);
            }
            else
            {
                return null;
            }
        }

        public async Task<CoverResponse> CreateAsync(CreateCoverRequest cover)
        {
            var mappedModel = _mapper.Map<Cover>(cover);
            mappedModel.Id = Guid.NewGuid().ToString();
            var response = await _cosmosDbService.AddItemAsync(mappedModel);
            return _mapper.Map<CoverResponse>(response);
        }

        public async Task DeleteAsync(string id)
        {
            await _cosmosDbService.DeleteItemAsync(id);
        }

        private decimal ComputePremium(DateTime startDate, DateTime endDate, ECoverType coverType)
        {
            Dictionary<ECoverType, decimal> typeMultipliers = new Dictionary<ECoverType, decimal>
            {
                { ECoverType.Yacht, 1.1m },
                { ECoverType.PassengerShip, 1.2m },
                { ECoverType.Tanker, 1.5m },
            };

            decimal basePremiumPerDay = 1250;
            decimal multiplier = typeMultipliers.ContainsKey(coverType) ? typeMultipliers[coverType] : 1.3m;

            decimal totalPremium = 0m;
            int insuranceLength = (endDate - startDate).Days;

            for (int day = 0; day < insuranceLength; day++)
            {
                if (day < 30)
                {
                    totalPremium += basePremiumPerDay * multiplier;
                }
                else if (day < 180)
                {
                    decimal discount = (day < 180) ? (coverType == ECoverType.Yacht ? 0.05m : 0.02m) : 0m;
                    totalPremium += (basePremiumPerDay * (1 - discount)) * multiplier;
                }
                else
                {
                    decimal discount = (coverType == ECoverType.Yacht) ? 0.03m : 0.01m;
                    totalPremium += (basePremiumPerDay * (1 - discount)) * multiplier;
                }
            }

            return totalPremium;
        }



    }
}
