﻿using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;

namespace Services
{
    public class ClaimsService : IClaimsService
    {
        private readonly IMapper _mapper;
        private readonly ICosmosDbService<Claim> _cosmosDbService;

        public ClaimsService(IMapper mapper,
                            ICosmosDbService<Claim> cosmosDbService)
        {
            _mapper = mapper;
            _cosmosDbService = cosmosDbService;
        }

        public async Task<IEnumerable<ClaimResponse>> GetClaimsAsync()
        {
            var response = await _cosmosDbService.GetItemsAsync("SELECT * FROM c");
            return _mapper.Map<IEnumerable<ClaimResponse>>(response);
        }

        public async Task<ClaimResponse> GetClaimAsync(string id)
        {
            var response = await _cosmosDbService.GetItemAsync(id);
            if (response != null)
            {
                return _mapper.Map<ClaimResponse>(response);
            }
            else
            {
                return null;
            }
        }

        public async Task<ClaimResponse> AddItemAsync(CreateClaimRequest claim)
        {
            var mappedModel = _mapper.Map<Claim>(claim);
            mappedModel.Id = Guid.NewGuid().ToString();
            var response = await _cosmosDbService.AddItemAsync(mappedModel);
            return _mapper.Map<ClaimResponse>(response);
        }

        public async Task DeleteItemAsync(string id)
        {
            await _cosmosDbService.DeleteItemAsync(id);
        }
    }
}
