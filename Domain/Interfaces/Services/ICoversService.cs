﻿using Domain.Entities;
using Domain.Models;
using Domain.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Services
{
    public interface ICoversService
    {
        Task<CoverResponse> CreateAsync(CreateCoverRequest cover);
        Task<IEnumerable<CoverResponse>> GetAsync();
        Task<CoverResponse> GetAsync(string id);
        Task DeleteAsync(string id);
        Task<decimal> ComputePremiumAsync(DateTime startDate, DateTime endDate, ECoverType coverType);
    }
}
