﻿namespace Domain.Interfaces.Services
{
    public interface IAuditorService
    {
        void AuditClaim(string id, string httpRequestType);
        void AuditCover(string id, string httpRequestType);
    }
}
