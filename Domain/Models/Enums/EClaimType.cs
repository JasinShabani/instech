﻿namespace Domain.Models.Enums
{ 
    public enum EClaimType
    {
        Collision = 0,
        Grounding = 1,
        BadWeather = 2,
        Fire = 3
    }
}