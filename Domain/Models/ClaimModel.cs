﻿using Domain.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ClaimModel
    {
        public string CoverId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public EClaimType Type { get; set; }
        public decimal DamageCost { get; set; }
    }
    public class CreateClaimRequest : ClaimModel
    {
    }
    public class ClaimResponse : ClaimModel
    {
        public string Id { get; set; }
    }
}
