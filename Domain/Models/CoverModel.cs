﻿using Domain.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class CoverModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ECoverType Type { get; set; }
        public decimal Premium { get; set; }
    }
    public class CreateCoverRequest : CoverModel
    {
    }
    public class CoverResponse : CoverModel
    {
        public string Id { get; set; }
    }
}
