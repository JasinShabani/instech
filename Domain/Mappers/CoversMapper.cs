﻿using AutoMapper;
using Domain.Entities;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Mappers
{
    public class CoversMapper : Profile
    {
        public CoversMapper()
        {
            CreateMap<Cover, CoverResponse>().ReverseMap();
            CreateMap<Cover, CreateCoverRequest>().ReverseMap();
        }
    }
}
