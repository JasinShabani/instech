﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Claims.Controllers;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Domain.Entities;

namespace ClaimsTests.Controllers.Tests
{
    [TestClass]
    public class ClaimsControllerTests
    {
        private Mock<ILogger<ClaimsController>> _loggerMock;
        private Mock<IClaimsService> _claimsServiceMock;
        private Mock<ICoversService> _coversServiceMock;
        private Mock<IAuditorService> _auditorServiceMock;

        [TestInitialize]
        public void Initialize()
        {
            _loggerMock = new Mock<ILogger<ClaimsController>>();
            _claimsServiceMock = new Mock<IClaimsService>();
            _coversServiceMock = new Mock<ICoversService>();
            _auditorServiceMock = new Mock<IAuditorService>();
        }

        [TestMethod]
        public async Task CreateAsync_InvalidClaim_ReturnsBadRequest()
        {
            // Arrange
            var invalidClaim = new CreateClaimRequest
            {
                DamageCost = 200000,
                Created = DateTime.Now,
                CoverId = "invalid-cover-id" 
            };

            _coversServiceMock
                .Setup<Task<CoverResponse>>(mock => mock.GetAsync(It.IsAny<string>()))
                .ReturnsAsync(new CoverResponse());

            var controller = new ClaimsController(
                _loggerMock.Object,
                _claimsServiceMock.Object,
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.CreateAsync(invalidClaim);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task CreateAsync_ValidClaim_ReturnsOk()
        {
            // Arrange 
            var validCover = new CoverResponse
            {
                Id = "valid-cover-id",
                StartDate = DateTime.Now.AddMonths(-3),
                EndDate = DateTime.Now.AddMonths(3),
            };

            _coversServiceMock
                .Setup<Task<CoverResponse>>(mock => mock.GetAsync(validCover.Id))
                .ReturnsAsync(validCover);

            var validClaim = new CreateClaimRequest
            {
                DamageCost = 50000,
                Created = DateTime.Now,
                CoverId = validCover.Id 
            };

            _claimsServiceMock.Setup(mock => mock.AddItemAsync(validClaim)).ReturnsAsync(new ClaimResponse());

            var controller = new ClaimsController(
                _loggerMock.Object,
                _claimsServiceMock.Object,
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.CreateAsync(validClaim);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }

        [TestMethod]
        public async Task GetAsync_ReturnsClaims()
        {
            // Arrange
            var claims = new List<ClaimResponse>
            {
                new ClaimResponse
                {
                    Id = "1",
                    DamageCost = 50000,
                    Created = DateTime.Now,
                    CoverId = "valid-cover-id"
                },
                new ClaimResponse
                {
                    Id = "2",
                    DamageCost = 60000,
                    Created = DateTime.Now.AddMonths(-1),
                    CoverId = "another-cover-id"
                }
            };

            _claimsServiceMock.Setup(mock => mock.GetClaimsAsync()).ReturnsAsync(claims);


            var controller = new ClaimsController(
                _loggerMock.Object,
                _claimsServiceMock.Object,
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.GetAsync();
            if (result.Result is OkObjectResult okObjectResult)
            {
                if (okObjectResult.Value is IEnumerable<ClaimResponse> claimResponses)
                {
                    List<ClaimResponse> coverList = claimResponses.ToList();
                    CollectionAssert.AreEqual(claims.ToList(), coverList);
                }
                else
                {
                    Assert.Fail();
                }
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task DeleteAsync_ValidClaim_ReturnsOk()
        {
            // Arrange
            var claimIdToDelete = "claim-id-to-delete";


            _claimsServiceMock
                .Setup(mock => mock.DeleteItemAsync(claimIdToDelete))
                .Returns(Task.CompletedTask); 

            var controller = new ClaimsController(
                _loggerMock.Object,
                _claimsServiceMock.Object,
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.DeleteAsync(claimIdToDelete);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }

    }
}
