﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Claims.Controllers;
using Domain.Interfaces.Services;
using Domain.Models;
using Domain.Models.Enums;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ClaimsTests.Controllers.Tests
{
    [TestClass]
    public class CoversControllerTests
    {
        private Mock<ILogger<CoversController>> _loggerMock;
        private Mock<ICoversService> _coversServiceMock;
        private Mock<IAuditorService> _auditorServiceMock;

        [TestInitialize]
        public void Initialize()
        {
            _loggerMock = new Mock<ILogger<CoversController>>();
            _coversServiceMock = new Mock<ICoversService>();
            _auditorServiceMock = new Mock<IAuditorService>();
        }

        [TestMethod]
        public async Task ComputePremiumAsync_ValidInput_ReturnsOk()
        {
            // Arrange
            var startDate = DateTime.Now;
            var endDate = startDate.AddMonths(6);
            var coverType = ECoverType.Yacht;
            var expectedPremium = 75000.0m; // Adjust this value based on your computation

            _coversServiceMock
                .Setup(mock => mock.ComputePremiumAsync(startDate, endDate, coverType))
                .ReturnsAsync(expectedPremium);

            var controller = new CoversController(
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.ComputePremiumAsync(startDate, endDate, coverType);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = result as OkObjectResult;
            Assert.AreEqual(expectedPremium, okResult.Value);
        }

        [TestMethod]
        public async Task GetAsync_ReturnsCovers()
        {
            // Arrange
            var covers = new List<CoverResponse>
            {
                new CoverResponse
                {
                    Id = "1",
                    StartDate = DateTime.Now.AddMonths(-2),
                    EndDate = DateTime.Now.AddMonths(6),
                    Type = ECoverType.Yacht,
                    Premium = 75000.0m
                },
                new CoverResponse
                {
                    Id = "2",
                    StartDate = DateTime.Now.AddMonths(-1),
                    EndDate = DateTime.Now.AddMonths(7),
                    Type = ECoverType.PassengerShip,
                    Premium = 60000.0m
                }
            };

            _coversServiceMock.Setup(mock => mock.GetAsync()).ReturnsAsync(covers);

            var controller = new CoversController(
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.GetAsync();
            if (result.Result is OkObjectResult okObjectResult)
            {
                if (okObjectResult.Value is IEnumerable<CoverResponse> coverResponses)
                {
                    List<CoverResponse> coverList = coverResponses.ToList();
                    CollectionAssert.AreEqual(covers.ToList(), coverList);
                }
                else
                {
                    Assert.Fail();
                }
            }
            else
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task CreateAsync_ValidCover_ReturnsOk()
        {
            // Arrange
            var validCover = new CreateCoverRequest
            {
                StartDate = DateTime.Now.AddDays(1),
                EndDate = DateTime.Now.AddMonths(6),
                Type = ECoverType.PassengerShip,
                Premium = 60000.0m
            };

            _coversServiceMock.Setup(mock => mock.CreateAsync(validCover)).ReturnsAsync(new CoverResponse
            {
                Id = "new-cover-id",
                StartDate = validCover.StartDate,
                EndDate = validCover.EndDate,
                Type = validCover.Type,
                Premium = validCover.Premium
            });

            var controller = new CoversController(
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.CreateAsync(validCover);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = result as OkObjectResult;
            var resultCover = okResult.Value as CoverResponse;
            Assert.AreEqual("new-cover-id", resultCover.Id);
        }

        [TestMethod]
        public async Task DeleteAsync_ValidCover_ReturnsOk()
        {
            // Arrange
            var coverIdToDelete = "cover-id-to-delete";

            _coversServiceMock
                .Setup(mock => mock.DeleteAsync(coverIdToDelete))
                .Returns(Task.CompletedTask);

            var controller = new CoversController(
                _coversServiceMock.Object,
                _auditorServiceMock.Object);

            // Act
            var result = await controller.DeleteAsync(coverIdToDelete);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkResult));
        }
    }
}
