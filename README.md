Completed all the tasks for the Insurance Claims Handling project:

Task 1 (Code Refactoring and SOLID Principles): I've refactored the codebase to follow SOLID principles, introduced proper layering, and added documentation for improved clarity and maintainability.

Task 2 (Data Validation): Data validation rules have been implemented for Claims and Cover entities to ensure integrity and compliance with specified constraints.

Task 3 (Asynchronous Processing): I've enhanced the auditing process by enabling asynchronous execution of database commands, which has significantly improved system responsiveness. While Azure managed services were considered, I've currently implemented an in-memory solution.

Task 4 (Unit Testing): In addition to the existing test, I've added mandatory unit tests to validate the functionality and reliability of the application.

Task 5 (Premium Computation Logic): I've thoroughly reviewed and improved the premium computation formula, incorporating all specified criteria for different object types and insurance period durations.

Notably, I've leveraged Azure for various aspects of the project:

Azure Key Vault for secure secret management.
Azure Cosmos DB for data storage.
Microsoft SQL Server for relational data storage.

[Here you are the swagger Link](https://claims20230913173913.azurewebsites.net/swagger/index.html).
