using Azure.Identity;
using Claims.Auditing;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.Interfaces.Services;
using Microsoft.Azure.Cosmos;
using Microsoft.EntityFrameworkCore;
using Repositories;
using Services;
using System.Text.Json.Serialization;
using Azure.Security.KeyVault.Secrets;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers().AddJsonOptions(x =>
{
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// Load configuration
var configuration = new ConfigurationBuilder()
    .SetBasePath(builder.Environment.ContentRootPath)
    .AddJsonFile("appsettings.Development.json")
    .AddEnvironmentVariables()
    .Build();

builder.Services.AddSingleton<IConfiguration>(configuration);


string GetClaimPartitionKey(Claim claim)
{
    return claim.Id;
}

string GetCoverPartitionKey(Cover cover)
{
    return cover.Id; 
}
var credential = new ClientSecretCredential(builder.Configuration["AzureVaultKeys:TenantId"], builder.Configuration["AzureVaultKeys:ClientId"], builder.Configuration["AzureVaultKeys:ClientSecret"]);
var client = new SecretClient(new Uri(builder.Configuration["AzureVaultKeys:KeyVaultUrl"]), credential);

KeyVaultSecret cosmosDbAccount = client.GetSecretAsync(builder.Configuration["CosmosDb:Account"]).Result;
KeyVaultSecret cosmosDbKey = client.GetSecretAsync(builder.Configuration["CosmosDb:Key"]).Result;
KeyVaultSecret cosmosDatabaseName = client.GetSecretAsync(builder.Configuration["CosmosDb:DatabaseName"]).Result;
KeyVaultSecret cosmosContainerNameClaim = client.GetSecretAsync(builder.Configuration["CosmosDb:ContainerNameClaim"]).Result;
KeyVaultSecret cosmosContainerNameCover = client.GetSecretAsync(builder.Configuration["CosmosDb:ContainerNameCover"]).Result;
KeyVaultSecret mssqlConnectionString = client.GetSecretAsync(builder.Configuration["ConnectionStrings:DefaultConnection"]).Result;

builder.Services.AddDbContext<AuditContext>(options => options.UseSqlServer(mssqlConnectionString.Value));
builder.Services.AddScoped<ICosmosDbService<Claim>>(provider =>
{
    var claimContainerTask = InitializeCosmosClientInstanceAsync(cosmosDatabaseName.Value, cosmosDbAccount.Value, cosmosDbKey.Value, cosmosContainerNameClaim.Value);
    return new CosmosDbService<Claim>(claimContainerTask.Result, GetClaimPartitionKey);
});

builder.Services.AddScoped<ICosmosDbService<Cover>>(provider =>
{
    var coverContainerTask = InitializeCosmosClientInstanceAsync(cosmosDatabaseName.Value, cosmosDbAccount.Value, cosmosDbKey.Value, cosmosContainerNameCover.Value);
    return new CosmosDbService<Cover>(coverContainerTask.Result, GetCoverPartitionKey);
});

// Rest of the service registrations...
builder.Services.AddScoped<IClaimsService, ClaimsService>();
builder.Services.AddScoped<ICoversService, CoversService>();
builder.Services.AddScoped<IAuditorService, AuditorService>();
builder.Services.AddScoped<IAuditorRepository, AuditorRepository>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
/*if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}*/
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<AuditContext>();
    context.Database.Migrate();
}

app.Run();

static async Task<Container> InitializeCosmosClientInstanceAsync(string cosmosDatabaseName, string cosmosAccount, string cosmosKey, string containerName)
{
    string databaseName = cosmosDatabaseName;
    string account = cosmosAccount;
    string key = cosmosKey;

    CosmosClient client = new CosmosClient(account, key);
    DatabaseResponse database = await client.CreateDatabaseIfNotExistsAsync(databaseName);
    await database.Database.CreateContainerIfNotExistsAsync(containerName, "/id");
    Container container = client.GetContainer(databaseName, containerName);

    return container;
}
