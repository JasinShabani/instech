using Azure;
using Claims.Auditing;
using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Claims.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClaimsController : ControllerBase
    {
        private readonly ILogger<ClaimsController> _logger;
        private readonly IClaimsService _claimsService;
        private readonly ICoversService _coversService; // Inject the CoversService
        private readonly IAuditorService _auditorService;

        public ClaimsController(
            ILogger<ClaimsController> logger,
            IClaimsService claimsService,
            ICoversService coversService, // Inject the CoversService
            IAuditorService auditorService)
        {
            _logger = logger;
            _claimsService = claimsService;
            _coversService = coversService;
            _auditorService = auditorService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClaimResponse>>> GetAsync()
        {
            var response = await _claimsService.GetClaimsAsync();
            var claimIds = string.Join(",", response.Select(claim => claim.Id));
            _auditorService.AuditClaim(claimIds, "GET");
            return Ok(response); // Ensure that the response is returned correctly
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(CreateClaimRequest claim)
        {
            if (claim.DamageCost > 100000)
            {
                ModelState.AddModelError("DamageCost", "DamageCost cannot exceed 100,000");
                return BadRequest(ModelState);
            }
            var cover = await _coversService.GetAsync(claim.CoverId);

            if (cover == null || claim.Created < cover.StartDate || claim.Created > cover.EndDate)
            {
                ModelState.AddModelError("Created", "Created date must be within the period of the related Cover");
                return BadRequest(ModelState);
            }

            var response = await _claimsService.AddItemAsync(claim);
            _auditorService.AuditClaim(response.Id, "POST");
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            await _claimsService.DeleteItemAsync(id);
            _auditorService.AuditClaim(id, "DELETE");
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ClaimResponse>> GetAsync(string id)
        {
            var response = await _claimsService.GetClaimAsync(id);
            if(response == null)
            {
                return NotFound("Claims with this ID couldn't be found");
            }
            _auditorService.AuditClaim(response.Id, "GET");
            return response;
        }
    }
}
