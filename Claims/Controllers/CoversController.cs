using Domain.Entities;
using Domain.Interfaces.Services;
using Domain.Models;
using Domain.Models.Enums;
using Microsoft.AspNetCore.Mvc;

namespace Claims.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoversController : ControllerBase
    {
        private readonly ICoversService _coversService;
        private readonly IAuditorService _auditorService;

        public CoversController(ICoversService coversService, IAuditorService auditorService)
        {
            _coversService = coversService;
            _auditorService = auditorService;
        }

        [HttpPost]
        [Route("compute-premium")]
        public async Task<ActionResult> ComputePremiumAsync(DateTime startDate, DateTime endDate, ECoverType coverType)
        {
            var result = await _coversService.ComputePremiumAsync(startDate, endDate, coverType);
            _auditorService.AuditCover("computePremium", "POST");
            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CoverResponse>>> GetAsync()
        {
            var covers = await _coversService.GetAsync();
            var coverIds = string.Join(",", covers.Select(cover => cover.Id));
            _auditorService.AuditCover(coverIds, "GET");
            return Ok(covers);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CoverResponse>> GetAsync(string id)
        {
            var response = await _coversService.GetAsync(id);
            if (response == null)
            {
                return NotFound("Claims with this ID couldn't be found");
            }
            _auditorService.AuditCover(response?.Id, "GET");
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromBody] CreateCoverRequest cover)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            if (cover.StartDate < DateTime.Now.Date)
            {
                ModelState.AddModelError("StartDate", "StartDate cannot be in the past.");
                return BadRequest(ModelState);
            }

            var insurancePeriod = cover.EndDate - cover.StartDate;
            if (insurancePeriod.Days > 365)
            {
                ModelState.AddModelError("EndDate", "Total insurance period cannot exceed 1 year.");
                return BadRequest(ModelState);
            }

            var result = await _coversService.CreateAsync(cover);
            _auditorService.AuditCover(result.Id, "POST");
            return Ok(result);
        }                       

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(string id)
        {
            try
            {
                await _coversService.DeleteAsync(id);
                _auditorService.AuditCover(id, "DELETE");
                return Ok();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("404")) 
                {
                    return NotFound("Cover with this ID couldn't be found");
                }
                else
                {
                    return StatusCode(500, "An error occurred while deleting the resource.");
                }
            }
        }
    }
}
